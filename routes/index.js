var express = require('express');
var router = express.Router();
const db = require('../data/data')

/* GET home page. */
router.get('/login', function(req, res, next) {
  res.render('login' ) 
});
router.get('/index', function(req, res, next) {
  res.render('index' );
});
router.get('/info/:id', function(req, res, next) {
  res.render('info' , {iduser : req.params.id} );
});
router.get('/testapi' , (req,res)=>{
  res.json(db.arr)
})


module.exports = router;
