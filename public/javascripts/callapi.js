
function check() {
  //document.getElementById('message').innerHTML = "checking";
  const data = {
    username: document.getElementById('email').value,
    password: document.getElementById('password').value
  };
  if(data.password === "" || data.username === "") {
    alert("Sai tài khoản hoặc mật khẩu. Nhập lại!");
    return
  }
  const header = {
    headers: {
      // 'Accept': 'application/json', 'content-type': 'application/json; charset=UTF-8'
      'Accept': '*/*',
      'Content-Type': 'application/json',
      
    },
    body: JSON.stringify(data),
    method: "POST",
    mode: "cors"
  };

  fetch("http://45.76.151.147:8091/api/auth/login", header)
    .then(function (response) {   
      return response.json();
    })
    .then(function (result) {
    let token = `${result.data.token}`; 
      console.log(result);
     let email = result.data.email;
     localStorage.setItem('token' , token)
      window.location = `http://localhost:3000/index`;
    })
    .catch(function (error) {
      console.log('Request failed', error);
      alert("Sai tài khoản hoặc mật khẩu. Nhập lại!");
    });
}