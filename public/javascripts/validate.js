function Validator(selecter) {

    function getParent(element, selector) {
        while (element.parentElement) {
            if (element.parentElement.matches(selector)) {
                return element.parentElement
            }
            element = element.parentElement
        }
    }


    var formRules = {}

    var ValidatorRules = {
        required: function (value) {
            return value.trim() ? undefined : 'Không được để trống'
        },
        min: function (min) {
            return function (value) {
                return value.length >= min ? undefined : `Vui lòng nhập ít nhất ${min} ký tự`;
            }
        },
    }

    var form = document.querySelector(selecter);

    if (form) {
        var inputs = form.querySelectorAll('[name][rules]')

        for (var input of inputs) {
            //split |
            var rules = input.getAttribute('rules').split('|')

            for (var rule of rules) {

                // split :
                var ruleHasValue = rule.includes(':')
                var infor

                if (ruleHasValue) {
                    infor = rule.split(':')
                    rule = infor[0]
                }

                var ruleFunc = ValidatorRules[rule]
                if (ruleHasValue) {
                    ruleFunc = ruleFunc(infor[1])
                }

                if (Array.isArray(formRules[input.name])) {
                    formRules[input.name].push(ruleFunc)
                } else {
                    formRules[input.name] = [ruleFunc]
                }
            }

            // lang nghe su kien
            input.oninput = handleClearError;
            input.onblur = handleValidate;

        }

        // thực hiện validate
        function handleValidate(event) {
            var rules = formRules[event.target.name]
            var errorMessage;

            for (var rule of rules) {
                errorMessage = rule(event.target.value)
                if (errorMessage) break;
            }

            // hiển thị lỗi ra ui
            if (errorMessage) {
                var formInput = getParent(event.target, '.formInput')

                if (formInput) {
                    formInput.classList.add('invalid')
                    var formMessage = formInput.querySelector('.form-message')
                    formMessage.innerHTML = errorMessage
                }
            }
            return !errorMessage;
        }

        // thực hiện clear error message
        function handleClearError(event) {
            var formInput = getParent(event.target, '.formInput')

            if (formInput.classList.contains('invalid')) {
                formInput.classList.remove('invalid')
                var formMessage = formInput.querySelector('.form-message')
                if (formMessage) {
                    formMessage.innerText = '';
                }
            }
        }

        form.onsubmit = function (event) {
            event.preventDefault();
            var inputs = form.querySelectorAll('[name][rules]');
            var isValid = true;

            for (var input of inputs) {

                // cho giong var rules = formRules[event.target.name];
                // handleValidate({target: input})
                if (!handleValidate({ target: input })) {
                    isValid = false;;
                }
            }
        }
    }

}