
function block(){
      let btnBlock = document.getElementsByClassName('btnblock') //lấy tất cả danh sách cần block
      for(let i =0 ; i< btnBlock.length ; i++){
      btnBlock[i].onclick = async () =>{
        let r = confirm('ban co muon xoa ?')
        if(r === true){
          let response = await fetch(`http://167.179.70.213:3000/auth/report?id=${btnBlock[i].value}` , {
            method : 'POST' ,
          headers : {
            token : localStorage.getItem('token')
          }
          })
          let data = await response.json()
          alert(data.statusMessage);

        }
    
      }
    }
}
function paganation() {
  let pagenumber = document.getElementsByClassName('pagenumber')
  for (let i = 0; i < pagenumber.length; i++) {
    pagenumber[i].onclick = async () => {
      const header = {
        headers: {
          'Accept': '*/*',
          'Content-Type': 'application/json',
          'token': localStorage.getItem('token'),
          method: "GET",
          mode: "cors"
        }
      }
      let respone = await fetch(`http://167.179.70.213:3000/information?page=${pagenumber[i].value}`, header)
      let dataapi = await respone.json()
      document.getElementById('data').innerHTML = ''
      dataapi.data.forEach((i) => {
        document.getElementById('data').innerHTML +=
          `
        <tr>              
        <td>${i.id ? i.id : ""}</td>
        <td>${i.first_name ? i.first_name : ""} ${i.last_name ? i.last_name : ""}</td>
        <td>${parseInt(i.gender) === 0 ? 'Nữ' : 'Nam'}</td>
              
        <td>${i.email ? i.email : ""}</td>
        <td>
        <button class="btnDetail" value=${i.id}>Chi tiet</button>
        <button class="btnblock"  value="${i.id}">Khoá</button>
      </tr>
        `
      })
      detailButton()
      block()
    }
  }
 

}


let dataSlice = []


//get api



async function loadData() {
  
  let data = '';
  const headers = {
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json',
      'token': localStorage.getItem('token'),
      method: "GET",
      mode: "cors"
    }
  }
  let respone = await fetch("http://167.179.70.213:3000/information?page=1", headers)
  let dataapi = await respone.json()



  dataSlice = dataapi.data
  dataSlice.forEach((i) => {

    data += `
      <tr>              
      <td>${i.id ? i.id : ""}</td>
      <td>${i.first_name ? i.first_name : ""} ${i.last_name ? i.last_name : ""}</td>
      <td>${parseInt(i.gender) === 0 ? 'Nữ' : 'Nam'}</td>
              
      <td>${i.email ? i.email : ""}</td>
       
        <td>
          <button class="btnDetail" value=${i.id}>Chi tiet</button>
          <button class="btnblock"  value="${i.id}">Khoá</button>
        </td>  
      </tr>`

    document.getElementById('data').innerHTML = data;

  })

  loadPagination()
  detailButton()
  block()
  

}
loadData();

async function loadPagination(){
  let pagination = document.getElementById('pagination')
  const header = {
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json',
      'token': localStorage.getItem('token'),
      method: "GET",
      mode: "cors"
    }
  }
  let response = await fetch("http://167.179.70.213:3000/listAccount", header)
  let dataListAccount = await response.json()
  localStorage.setItem('item' , JSON.stringify(dataListAccount.data))
  let totalPage = Math.ceil(dataListAccount.data.length / 10)
  pagination.innerHTML = ""
  for(let i = 1  ; i <= totalPage ; i++){
   pagination.innerHTML += `
    <button class="pagenumber" value="${i}">${i}</button>
    `
  }
  paganation()
}


async function searchData(event) {
  let data = document.getElementById('data')
  let query = document.getElementById('search').value.toLowerCase()
  let pagination = document.getElementById('pagination')
 
  if(query && query !== ""){
    const header = {
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token'),
        method: "GET",
        mode: "cors"
      }
    }
    
    let respone = await fetch(`http://167.179.70.213:3000/information/findUser?email=${query}`, header)
    let dataapi = await respone.json()
  
    let dataSearch = dataapi.data
    localStorage.setItem('item' , JSON.stringify(dataSearch))

    data.innerHTML = ""
    dataSearch.slice(0,10).forEach((i) => {
  
      data.innerHTML += `
        <tr>              
        <td>${i.id ? i.id : ""}</td>
        <td>${i.first_name ? i.first_name : ""} ${i.last_name ? i.last_name : ""}</td>
        <td>${parseInt(i.gender) === 0 ? 'Nữ' : 'Nam'}</td>
              
        <td>${i.email ? i.email : ""}</td>
         
          <td>
            <button class="btnDetail" value=${i.id}>Chi tiet</button>
            <button class="btnblock"  value="${i.id}">Khoá</button>
          </td>  
        </tr>`
  
  
    })

    let totalPage = Math.ceil(dataSearch.length / 10)
    pagination.innerHTML = ""
    for(let i =1 ; i<= totalPage ; i++){
      pagination.innerHTML +=`
      <button class='searchpage' value="${i}" >${i}</button>
      `
    }
    paginationForSearch()
    detailButton()
    block()

  }
  else{
    loadData()
  }
  detailButton()
}

function paginationForSearch(){
  let btnPaginationForSearch = document.getElementsByClassName("searchpage")
  let dataHTML = document.getElementById('data')
  let data = JSON.parse(localStorage.getItem('item'))
  for(let i = 0 ; i< btnPaginationForSearch.length ; i++){
    btnPaginationForSearch[i].onclick = ()=>{
          let start =(parseInt(btnPaginationForSearch[i].value) - 1) * 10
          let end = start + 10
          dataHTML.innerHTML = ""
          data.slice(start , end).forEach((i)=>{
            
              dataHTML.innerHTML += `
              <tr>              
              <td>${i.id ? i.id : ""}</td>
              <td>${i.first_name ? i.first_name : ""} ${i.last_name ? i.last_name : ""}</td>
              <td>${parseInt(i.gender) === 0 ? 'Nữ' : 'Nam'}</td>
              
              <td>${i.email ? i.email : ""}</td>
               
                <td>
                  <button class="btnDetail" value=${i.id}>Chi tiet</button>
                  <button class="btnblock"  value="${i.id}">Khoá</button>
                </td>  
              </tr>
              `
          })
          console.log(i.gender)
    }
  }
  detailButton()
  block()
}


function detailButton() {
  let btnDetail = document.getElementsByClassName('btnDetail')
  for (let i = 0; i < btnDetail.length; i++) {
    btnDetail[i].onclick = () => {
      window.location.href = `/info/${btnDetail[i].value}`
    }
  }
}

// function searchData() {
//   let data = '';
//   let query = document.getElementById('search').value.toLowerCase()
//   var datafilter = []

//   if(query !== "") {
//     datafilter = dataSlice.filter(item => item.email.includes(query))
//     console.log(datafilter);
//   }
//   else if(query === "" && datafilter.length === 0) {
//     datafilter = dataSlice
//   }
//   else if(datafilter.length === 0 && query !== "") {
//     datafilter = []
//   }
//   if(datafilter.length !== 0) {

//     localStorage.setItem('item', JSON.stringify(datafilter))
//     datafilter.forEach((i) => {
//       data += `
//         <tr>              
//           <td>${i.email ? i.email : "" }</td>
//           <td>${i.gender ? i.gender : ""}</td>
//           <td>${i.id ? i.id : ""}</td>
//           <td>${i.first_name ? i.first_name : ""} ${i.last_name ? i.last_name : ""}</td>
//           <td>${i.birthday ? i.birthday : ""}</td>
//           <td>${i.university_name ? i.university_name : ""}</td>
//           <td>${i.university_id?  i.university_id : ""} </td>
//           <td>${i.phone ? i.phone : ""}</td>

//           <td>
//             <button class="btnDetail" value=${i.id}>Chi tiet</button>
//             <input class="btnDelete" type="button" value="Xoá">
//           </td>  
//         </tr>`

//       document.getElementById('data').innerHTML = data;
//     })
//   } else {
//     data += ``

//       document.getElementById('data').innerHTML = data;
//   }
// }

// function searchData() {
//   let q = document.getElementById('search').value.toLowerCase()

//   let data = JSON.parse(localStorage.getItem('item'))
//   let numberPanagation = Math.ceil(data.length / 10)
//   let dataSlice = data.slice(0, 10);
//   if (q === "") {
//     let temp = ""
//     dataSlice.forEach((i) => {
//       temp += `
//       <tr>             
//       <td>${i.email ? i.email : "" }</td>
//         <td>${i.gender ? i.gender : ""}</td>
//         <td>${i.id ? i.id : ""}</td>
//         <td>${i.first_name ? i.first_name : ""} ${i.last_name ? i.last_name : ""}</td>
//         <td>${i.birthday ? i.birthday : ""}</td>
//         <td>${i.university_name ? i.university_name : ""}</td>
//         <td>${i.university_id?  i.university_id : ""} </td>
//         <td>${i.phone ? i.phone : ""}</td>
//         <td>
//         <button class="btnDetail" value=${i.id}>Chi tiet</button>
//         <input class="btnDelete" type="button" value="Xoá">
//         </td>  
//       </tr>`

//       document.getElementById('data').innerHTML = temp

//     })

//     document.getElementById('cuong').innerHTML = `<li class="page-item "><a id='previous' class="page-link" href="#" >Previous</a></li>`
//     for (let i = 1; i <= numberPanagation; i++) {
//       document.getElementById('cuong').innerHTML += `
//       <li class="page-item"> <a class="page-link" href="#">${i}</a></li>
//       `
//     }
//     document.getElementById('cuong').innerHTML += `<li class="page-item "><a id='next' class="page-link" href="#" >Next</a></li>`
//     paganation(data)
//     next(data)
//     previous(data)
//     return
//   }

//   let result = data.filter(i => i.Name.toLowerCase().indexOf(q) != -1);
//   dataSlice = result.slice(0, 10);
//   numberPanagation = Math.ceil(result.length / 10)
//   let temp2 = ""


//   dataSlice.forEach((i) => {
//     temp2 += `
//     <tr>             
//     <td>${i.email ? i.email : "" }</td>
//       <td>${i.gender ? i.gender : ""}</td>
//       <td>${i.id ? i.id : ""}</td>
//       <td>${i.first_name ? i.first_name : ""} ${i.last_name ? i.last_name : ""}</td>
//       <td>${i.birthday ? i.birthday : ""}</td>
//       <td>${i.university_name ? i.university_name : ""}</td>
//       <td>${i.university_id?  i.university_id : ""} </td>
//       <td>${i.phone ? i.phone : ""}</td>
//       <td>
//       <button class="btnDetail" value=${i.id}>Chi tiet</button>
//       <input class="btnDelete" type="button" value="Xoá">
//       </td>  
//     </tr>`
//     document.getElementById('data').innerHTML = temp2
//   })

//   document.getElementById('cuong').innerHTML = `<li class="page-item "><a id='previous' class="page-link" href="#" >Previous</a></li>`
//   for (let i = 1; i <= numberPanagation; i++) {
//     document.getElementById('cuong').innerHTML += `
//       <li class="page-item"> <a class="page-link" href="#">${i}</a></li>
//       `
//   }
//   document.getElementById('cuong').innerHTML += `<li class="page-item "><a id='next' class="page-link" href="#" >Next</a></li>`
//   paganation(result)
//   next(result)
//   previous(result)


// }


